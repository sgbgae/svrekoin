<?php
require_once "DB_Functions.php";
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);

if (isset($_POST['email'])) {
 
    $email = $_POST['email'];
    $user = $db->getUserSaldo($email);
   if ($user != false) {
        // user ditemukan
        $response["error"] = FALSE;
        $response["user"]["saldo"] = $user["saldo"];
        echo json_encode($response);
    } else {
        // user tidak ditemukan password/email salah
        $response["error"] = TRUE;
        $response["error_msg"] = "Login gagal. Password/Email salah";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Parameter ada yang kurang";
    echo json_encode($response);
}
?>
