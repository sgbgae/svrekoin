<?php
 
require_once 'DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['trx_to']) ) {
 
    $trx_to = $_POST['trx_to'];

        $user = $db->checkNama($trx_to);
        if ($user) {
            // simpan berhasil
            $response["error"] = FALSE;
            $response["nama"] = $user["nama"];
	    //$response = $user["nama"];		    	
            echo json_encode($response);
        } else {
            // gagal menyimpan user
            $response["error"] = TRUE;
            $response["error_msg"] = "Terjadi kesalahan saat melakukan pembacaan";
            echo json_encode($response);
        }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "periksa isian ada yang kurang";
    echo json_encode($response);
}
?>
