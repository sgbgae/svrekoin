<?php
class DB_Connect {
    private $conn;
 
    // koneksi ke database
    public function connect() {
        require_once 'config.php';
         
        // koneksi ke mysql database
        $this->conn = new mysqli(null, DB_USER, DB_PASSWORD, DB_DATABASE,null, DB_HOST);
				//$mysqli = new mysqli(null, "root", "pass@word1", "db_mahasiswa",null,"/cloudsql/database-sgb:us-central1:database-sgb");
				
         
        // return database handler
        return $this->conn;
    }
}
 
?>