<?php 
require __DIR__ . '/vendor/autoload.php';
require 'libs/NotORM.php'; 

use \Slim\App;

$app = new App();

$dbhost = ':/cloudsql/database-sgb:us-central1:database-sgb';
$dbuser = 'root';
$dbpass = 'pass@word1';
$dbname = 'db_mahasiswa';
$dbmethod = 'mysql:dbname=';

$dsn = $dbmethod.$dbname;
$pdo = new PDO($dsn, $dbuser, $dbpass);
$db  = new NotORM($pdo);

$app-> get('/', function(){
    echo "API Mahasiswa";
});

$app ->get('/semuadosen', function() use($app, $db){
	$dosen["error"] = false;
	$dosen["message"] = "Berhasil mendapatkan Kontak";
    foreach($db->tbl_user() as $data){
        $dosen['semuadosen'][] = array(
            'id' => $data['unique_id'],
            'nama' => $data['nama'],
            'matkul' => $data['email']
            );
    }
    echo json_encode($dosen);
});

$app ->get('/dosen/{nama}', function($request, $response, $args) use($app, $db){
    $dosen = $db->tbl_user()->where('unique_id',$args['nama']);
    $dosendetail = $dosen->fetch();

    if ($dosen->count() == 0) {
        $responseJson["error"] = true;
        $responseJson["message"] = "Kontak belum tersedia di database";
        $responseJson["nama"] = null;
        $responseJson["matkul"] = null;
        $responseJson["no_hp"] = null;
    } else {
        $responseJson["error"] = false;
        $responseJson["message"] = "Berhasil mengambil data";
        $responseJson["nama"] = $dosendetail['nama'];
        $responseJson["matkul"] = $dosendetail['unique_id'];
        $responseJson["no_hp"] = $dosendetail['email'];
    }

    echo json_encode($responseJson); 
});
// /*
$app ->get('/report', function() use($app, $db){
$response = array();
//    $trx_to = $_POST['trx_from'];
$trx_from = '59db4b29751407.90217284';
        $mysqli = new mysqli("localhost", "root", "pass@word1", "db_mahasiswa");
        if ($mysqli->connect_errno) {
                echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }

        $res2 = $mysqli->query("
SELECT  trx_id, trx_date, trx_nominal, trx_status, nama, unique_id, email
FROM transaction INNER JOIN tbl_user
ON transaction.trx_from = tbl_user.unique_id
where transaction.trx_to = '$trx_from'
GROUP BY trx_id
UNION ALL
SELECT  trx_id, trx_date, trx_nominal, trx_status, nama, unique_id, email
FROM transaction INNER JOIN tbl_user
ON transaction.trx_to = tbl_user.unique_id
where transaction.trx_from = '$trx_from'
GROUP BY trx_id
ORDER BY trx_id desc
");
	    $myArray['error']=FALSE;	
	    $myArray["message"] = "Berhasil mendapatkan data mata kuliah"; 
    while($row = $res2->fetch_array(MYSQL_ASSOC)) {
            $myArray['transaksi'][] = $row;
    }
    echo json_encode($myArray);
});
//*/
$app ->get('/matkul', function() use($app, $db){
    $dosen = $db->transaction()->order("trx_id desc");
    $dosendetail = $dosen->fetch();

    if ($dosen->count() == 0) {
        $responseJson["error"] = true;
        $responseJson["message"] = "Belum bertransaksi";
    } else {
        $responseJson["error"] = false;
        $responseJson["message"] = "Berhasil mendapatkan data";
        foreach($dosen as $data){
        $responseJson['semuamatkul'][] = array(
            'id' => $data['trx_id'],
            'nama_dosen' => 'Rp '.$data['trx_nominal'].',00',
            'matkul' => $data['trx_date']
            );
        }
    }
    echo json_encode($responseJson);
});

$app->post('/matkul', function($request, $response, $args) use($app, $db){
    $matkul = $request->getParams();
    $result = $db->tbl_matkul->insert($matkul);

    $responseJson["error"] = false;
    $responseJson["message"] = "Berhasil menambahkan ke database";
    echo json_encode($responseJson);
});

$app->delete('/matkul/{id}', function($request, $response, $args) use($app, $db){
    $matkul = $db->tbl_matkul()->where('id', $args);
    if($matkul->fetch()){
        $result = $matkul->delete();
        echo json_encode(array(
            "error" => false,
            "message" => "Matkul berhasil dihapus"));
    }
    else{
        echo json_encode(array(
            "error" => true,
            "message" => "Matkul ID tersebut tidak ada"));
    }
});

//run App
$app->run();
