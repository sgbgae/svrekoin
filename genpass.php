<?php
class DB_Functions {
	function __construct() {
	}

	// destructor
	function __destruct() {         
	}
		
  public function simpanUser($nama, $email, $password) {
			$uuid = uniqid('', true);
			$hash = $this->hashSSHA($password);
			$encrypted_password = $hash["encrypted"]; // encrypted password
			$salt = $hash["salt"]; // salt

			$mysqli = new mysqli(null, "root", "pass@word1", "db_mahasiswa",null,"/cloudsql/database-sgb:us-central1:database-sgb");
			if ($mysqli->connect_errno) {
				echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
			}
			
			if( $res = $mysqli->query("INSERT INTO db_mahasiswa.tbl_user(unique_id, nama, email, encrypted_password, salt) VALUES('$uuid', '$nama', '$email', '$encrypted_password', '$salt')")) {
				$res2 = $mysqli->query("SELECT * FROM db_mahasiswa.tbl_user WHERE email ='$email'");
				$user = $res2->fetch_assoc();
				return $user;
			} else {
				return false;
			}
    }

  public function generateIDPassword($password) {
		$uuid = uniqid('', true);
		$hash = $this->hashSSHA($password);
		$encrypted_password = $hash["encrypted"]; // encrypted password
		$salt = $hash["salt"]; // salt
		$newid = array();
		$newid['unique_id'] = $uuid;
		$newid['password'] = $password;
		$newid['encrypted'] = $encrypted_password;
		$newid['salt'] = $salt;
		return $newid;	
  }
		
	/**
	 * Encrypting password
	 * @param password
	 * returns salt and encrypted password
	 */
	public function hashSSHA($password) {

			$salt = sha1(rand());
			$salt = substr($salt, 0, 10);
			$encrypted = base64_encode(sha1($password . $salt, true) . $salt);
			$hash = array("salt" => $salt, "encrypted" => $encrypted);
			return $hash;
	}

	/**
	 * Decrypting password
	 * @param salt, password
	 * returns hash string
	 */
	public function checkhashSSHA($salt, $password) {
			$hash = base64_encode(sha1($password . $salt, true) . $salt);

			return $hash;
	}
}	
?>